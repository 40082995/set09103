from flask import Flask, render_template, request, flash, url_for, redirect, session, g
from functools import wraps
from passlib.hash import sha256_crypt
from wtforms import Form, BooleanField, TextField, PasswordField, validators
import gc
import sqlite3 as sql
from wtforms import Form
import os
import smtplib
from flask_mail import Mail, Message
from anime_management import Anime

ANIME_DICT = Anime()



app = Flask(__name__)
app.debug = True
app.secret_key = os.urandom(24)


app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER='smtp.mail.yahoo.com',
	MAIL_PORT=465,
	MAIL_USE_SSL=True,
	MAIL_USERNAME = 'rdnhayes@yahoo.co.uk',
	MAIL_PASSWORD = 'Superman13'
	)
mail = Mail(app)
app.config.from_object(__name__)

def get_db():
 db = getattr(g, 'db', None)
 if db is None:
	db = sqlite3.connect(DATABASE)
	g.db = db
 return db

@app.teardown_appcontext
def close_db_connection(exception):
 db = getattr(g, 'db', None )
 if db is not None:
	db.close() 

 
@app.route('/<path:urlpath>/')
@app.route('/')
def homepage(urlpath="/"):
 return render_template("index.html", ANIME_DICT = ANIME_DICT )

@app.route('/head/')
def head():
 return render_template("header.html", ANIME_DICT = ANIME_DICT )

 
@app.route("/chatroom/")
def chatbox():
    return render_template("chatroom.html")

@app.route("/demo-error/")
def demo_error():
    return render_template("404.html")	 
 
@app.route('/send-mail/')
def send_mail():
	try:
		msg = Message("Send Mail Tutorial!",
		  sender="rdnhayes@yahoo.co.uk",
		  recipients=["rdnhayes@gmail.com"])
		msg.body = "Hello!\nYou have sucessfully Signed for TheNewWorld\nNow enjoy the world of Anime :)"           
		mail.send(msg)
		flash("You have successfully Signed Up, An email as been sent to confirm your Account")
		return redirect(url_for("homepage"))
	except Exception, e:
		return(str(e))
 
@app.route('/anime/')
def anime():
 return render_template("anime.html", ANIME_DICT = ANIME_DICT) 

 
class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the Terms of Service and Privacy Notice (updated Jan 22, 2015)', [validators.Required()])

@app.route('/register/', methods=["GET","POST"])
def register_page():
    try:
        form = RegistrationForm(request.form)

        if request.method == "POST" and form.validate():
				username  = form.username.data
				email = form.email.data
				password = sha256_crypt.encrypt((str(form.password.data)))
				con = sql.connect('usernames.db')
				cur = con.cursor()
				
				x = ("SELECT * FROM users WHERE username = (?)",username)
                          

				if int(len(x)) < 0:
					flash("That username is already taken, please choose another")
					return render_template('register.html', form=form)

				else:
					cur.execute("INSERT INTO users (username, password, email, tracking) VALUES (?, ?, ?, ?)",
                          (username, password, email, "/",))
                
					con.commit()
					
					
					con.close()
					gc.collect()

					session['logged_in'] = True
					session['username'] = username

					return redirect(url_for('send_mail'))

        return render_template("register.html", form=form)

    except Exception as e:
        return(str(e))
		

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login_page'))

    return wrap
	
@app.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('homepage'))
	
@app.route('/login/', methods=["GET","POST"])	
def login_page():
    error = ''
    try:
			con = sql.connect('usernames.db')
			cur = con.cursor()
			if request.method == "POST":

				data = cur.execute("SELECT usersname FROM users",'username,')
            
				data = cur.fetchone()[2]

				if sha256_crypt.verify(request.form['password'], data):
					session['logged_in'] = True
					session['username'] = request.form['username']

					flash("You are now logged in")
					return redirect(url_for("homepage"))

			else:
					error = "Invalid credentials, try again."

			gc.collect()

			return render_template("login.html", error=error)

    except Exception as e:
        #flash(e)
        error = "Invalid credentials, try again."
        return render_template("login.html", error = error) 			

		
@app.route(ANIME_DICT["animedata"][0][1], methods=['GET', 'POST'])
def Gangsta():
    return render_template("animedata/Gangsta.html", curLink = ANIME_DICT["animedata"][0][1], curTitle=ANIME_DICT["animedata"][0][0],curIMAGE=ANIME_DICT["animedata"][0][2],curTRAILER=ANIME_DICT["animedata"][0][3])




@app.route(ANIME_DICT["animedata"][1][1], methods=['GET', 'POST'])
def Naruto():
 replies = {'Jack':'Awsome Anime',
			   'Erika':'Most definitely',
			   'Bob':'wow',
			   'Carl':'amazing!',}
 return render_template( "animedata/Naruto.html", curLink = ANIME_DICT["animedata"][1][1], curTitle=ANIME_DICT["animedata"][1][0],curIMAGE=ANIME_DICT["animedata"][1][2],curTRAILER=ANIME_DICT["animedata"][1][3], replies=replies)




@app.route(ANIME_DICT["animedata"][2][1], methods=['GET', 'POST'])
def Fatestay_night_Heavens_Fee():
    return render_template("animedata/Fate-stay-night-Heavens-Fee.html", curLink = ANIME_DICT["animedata"][2][1], curTitle=ANIME_DICT["animedata"][2][0],curIMAGE=ANIME_DICT["animedata"][2][2],curTRAILER=ANIME_DICT["animedata"][2][3])




@app.route(ANIME_DICT["animedata"][3][1], methods=['GET', 'POST'])
def Death_Note():
    return render_template("animedata/Death-Note.html", curLink = ANIME_DICT["animedata"][3][1], curTitle=ANIME_DICT["animedata"][3][0],curIMAGE=ANIME_DICT["animedata"][3][2],curTRAILER=ANIME_DICT["animedata"][3][3])




@app.route(ANIME_DICT["animedata"][4][1], methods=['GET', 'POST'])
def Attack_on_Titan():
    return render_template("animedata/Attack-on-Titan.html", curLink = ANIME_DICT["animedata"][4][1], curTitle=ANIME_DICT["animedata"][4][0],curIMAGE=ANIME_DICT["animedata"][4][2],curTRAILER=ANIME_DICT["animedata"][4][3])




@app.route(ANIME_DICT["animedata"][5][1], methods=['GET', 'POST'])
def Code_Geass_Lelouch_of_the_Rebellion():
    return render_template("animedata/Code-Geass-Lelouch-of-the-Rebellion.html", curLink = ANIME_DICT["animedata"][5][1], curTitle=ANIME_DICT["animedata"][5][0],curIMAGE=ANIME_DICT["animedata"][5][2],curTRAILER=ANIME_DICT["animedata"][5][3])

@app.errorhandler(404)
def page_not_found(e):
    try:
        gc.collect()
        rule = request.path
        if "anime" in rule or "bleach" in rule or "am-content" in rule or "an-login" in rule or "wp-login" in rule or "wp-admin" in rule or "charile" in rule or "tag" in rule or "wp-include" in rule or "style" in rule or "apple-touch" in rule or "genericons" in rule or "topics" in rule or "category" in rule or "show" in rule or "include" in rule or "trackback" in rule or "download" in rule or "viewtopic" in rule or "browserconfig" in rule:
            pass
        else:
            pass
        return render_template('404.html'), 404
    except Exception as e:
        return(str(e))

@app.errorhandler(500)
def page_not_found(e):
    return ("Ouch, looks like we're knocked out"), 500
	
	
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
