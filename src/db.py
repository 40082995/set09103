import sqlite3 as lite
import sys
	
con = lite.connect('usernames.db')

with con:
	cur = con.cursor()
	
	cur.execute("DROP TABLE IF EXISTS users")
	cur.execute("CREATE TABLE users(username TEXT, password TEXT, email TEXT, tracking TEXT )")
	