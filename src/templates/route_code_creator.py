from anime_management import Anime
ANIME_DICT = Anime()

FUNC_TEMPLATE = '''

@app.route(ANIME_DICT["CURRENTFILM"][CURRENTINDEX][1], methods=['GET', 'POST'])
def CURRENTTITLE():
    return render_template("CURRENTFILM/CURRENTHTML", curLink = ANIME_DICT["CURRENTFILM"][CURRENTINDEX][1], curTitle=ANIME_DICT["CURRENTFILM"][CURRENTINDEX][0],curIMAGE=ANIME_DICT["CURRENTFILM"][CURRENTINDEX][2],curTRAILER=ANIME_DICT["CURRENTFILM"][CURRENTINDEX][3])

'''

for each_topic in ANIME_DICT:
    #print(each_topic)

    index_counter = 0
    for eachele in ANIME_DICT[each_topic]:
        try:
            CURRENTHTML = (eachele[1]+'.html').replace("/","")
            CURRENTFILM = each_topic
            CURRENTTITLE = eachele[0].replace("-","_").replace(" ","_").replace(",","").replace("/","").replace(")","").replace("(","").replace(".","").replace("!","").replace(":","-").replace("'","")
            CURRENTINDEX = str(index_counter)
            NEXTINDEX = str(index_counter + 1)
            index_counter += 1

            print( FUNC_TEMPLATE.replace("CURRENTFILM",CURRENTFILM).replace("CURRENTINDEX",CURRENTINDEX).replace("CURRENTTITLE",CURRENTTITLE).replace("CURRENTHTML",CURRENTHTML).replace("NEXTINDEX",NEXTINDEX) )

        except Exception as e:
            print(str(e))	
